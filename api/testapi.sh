#!/bin/bash

PORT=8900
URL="http://localhost"
ROUTE=""

curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}

curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"localtest":{"Name": "TestData", "Value": "12345Blah"}}' \
    ${URL}:${PORT}/${ROUTE}

curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}

curl -i \
    -X PUT \
    -H "Content-Type: application/json" \
    --data '{"localtest":{"Name": "TestData2", "Value": "ZARF!"}}' \
    ${URL}:${PORT}/${ROUTE}

curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}
    
curl -i \
    -X DELETE \
    -H "Content-Type: application/json" \
    --data '"localtest"' \
    ${URL}:${PORT}/${ROUTE}

curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}
