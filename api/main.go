package main

import (
    "fmt"
    "github.com/gofiber/fiber/v2"
    "os"
    // "strings"
)

type StoreElement struct {
    Name  string `json:"Name"`
    Value string `json:"Value"`
}

const (
    DATAFILE = "./dataFile.gob"
)

var (
    app  *fiber.App
    DATA map[string]StoreElement
)

func init() {
    app = fiber.New()
}

func GetRoot(c *fiber.Ctx) error {
    return c.JSON(DATA)
}

/*
PostRoot
Takes a JSON post request formated such as:
{
    "uniqueKey": {
        "Name": "nameOfValue",
        "Value": "someValue"
    }
}
*/
func PostRoot(c *fiber.Ctx) error {
    var payload map[string]StoreElement

    if err := c.BodyParser(&payload); err != nil {
        return err
    }

    // Easiest approach in case multiple keys are sent in a single post
    for index, kvs := range payload {
        fmt.Println("Index: ", index)
        fmt.Println("KVS: ", kvs)
        key := index

        if !ADD(string(key), kvs) { // Add the data to the keyvalue store
            fmt.Println("Add operation failed!")
        } else {
            errSave := save()
            if errSave != nil {
                fmt.Println("Save after ADD failed")
                fmt.Println(errSave)
            }
        }
    }
    return c.JSON(payload)
}

/*
Delete item if found in the key value store
*/
func DelRoot(c *fiber.Ctx) error {
    var payload string

    if err := c.BodyParser(&payload); err != nil {
        return err
    }

    fmt.Println("Input Delete: ", payload)

    if !DELETE(payload) { // Delete the data to the keyvalue store
        fmt.Println("Delete operation failed!")
    } else {
        errSave := save()
        if errSave != nil {
            fmt.Println("Save after delete failed")
            fmt.Println(errSave)
        }
    }

    return c.JSON(payload)
}

// Updates current entry
func UpdateRoot(c *fiber.Ctx) error {
    var payload map[string]StoreElement

    if err := c.BodyParser(&payload); err != nil {
        return err
    }

    // Easiest approach in case multiple keys are sent in a single post
    for index, kvs := range payload {
        fmt.Println("Index: ", index)
        fmt.Println("KVS: ", kvs)
        key := index

        if !CHANGE(string(key), kvs) { // Change the data to the keyvalue store
            fmt.Println("Update operation failed!")
        } else {
            errSave := save()
            if errSave != nil {
                fmt.Println("Save after change failed")
                fmt.Println(errSave)
            }
        }
    }
    return c.JSON(payload)
}

func main() {
    PORT := ":8900"
    arguments := os.Args
    if len(arguments) == 2 {
        PORT = ":" + arguments[1]
    }

    errData := load()
    if errData != nil {
        fmt.Println(errData)
    }

    fmt.Printf("Using port number: %s\n", PORT)

    app.Get("/", GetRoot).Name("home")
    app.Post("/", PostRoot).Name("Write")
    app.Delete("/", DelRoot).Name("remove")
    app.Put("/", UpdateRoot).Name("update")

    app.Listen(PORT)
}
