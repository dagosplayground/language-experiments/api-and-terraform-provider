[[_TOC_]]

# Provider Example

This is to be a mock up of a custom provider for the api server written in the root folder. This project is only to be used as a simple proof of concept or devolve into some sort of code Rube Goldberg Machine for entertainement purposes only... unless it controls a Christmas tree/Unicorn.

# References
* [Writing Custom Terraform Providers](https://www.hashicorp.com/blog/writing-custom-terraform-providers)
* [Creating Terraform Providers for Anything](https://www.hashicorp.com/resources/creating-terraform-provider-for-anything)
